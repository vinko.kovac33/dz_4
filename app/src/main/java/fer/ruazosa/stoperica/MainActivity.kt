package fer.ruazosa.stoperica

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myViewModel =
                ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(TimerViewModel::class.java)

        myViewModel.resultOfDataFetch.observe(this, Observer {
            timerTextView.text = it
        })

        startButton.setOnClickListener {
            TimerRepository.stop = false
            myViewModel.fetchDataFromRepository()
        }

        pauseButton.setOnClickListener {
            if (TimerRepository.stop == false) {
                if (TimerRepository.pause == true) {
                    TimerRepository.pause = false
                    pauseButton.text = "PAUSE"
                    val toastMessage = Toast.makeText(applicationContext, "CONTINUED", Toast.LENGTH_LONG)
                    toastMessage.show()
                }
                else {
                    TimerRepository.pause = true
                    val toastMessage = Toast.makeText(applicationContext, "PAUSED", Toast.LENGTH_LONG)
                    toastMessage.show()
                    pauseButton.text = "CONTINUE"
                }
            }

        }

        stopButton.setOnClickListener {
            TimerRepository.stop = true
            TimerRepository.start = true
            TimerRepository.pause = false
            pauseButton.text = "PAUSE"
            val toastMessage = Toast.makeText(applicationContext, "STOPPED", Toast.LENGTH_LONG)
            toastMessage.show()
        }


    }
}