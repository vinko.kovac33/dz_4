package fer.ruazosa.stoperica

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TimerViewModel: ViewModel() {

    val resultOfDataFetch = MutableLiveData<String>()
    var fetchedResult: Int = 0

    fun fetchDataFromRepository() {
        viewModelScope.launch{
            withContext(Dispatchers.IO) {
                if (TimerRepository.start == true) {
                    fetchedResult = 0
                    TimerRepository.counter = 0
                    TimerRepository.start = false
                    while (true) {
                        if (TimerRepository.stop == true) {
                            break
                        }
                        if (TimerRepository.pause == false) {
                            TimerRepository.counter++
                            withContext(Dispatchers.Main) {
                                resultOfDataFetch.value = fetchedResult.toString()
                            }
                        }
                        fetchedResult = TimerRepository.fetchData(TimerRepository.counter)
                    }
                }
            }
        }
    }
}